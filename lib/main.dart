import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'content_list_tile.dart';
import 'gesture_detector.dart';

final List<String> notes = [
  "fluttermaster.com",
  "Update Android Studio to 3.3",
  "Implement ListView widget",
  "Demo ListView simplenote app",
  "Fixing app color",
  "Create new note screen",
  "Persist notes data",
  "Add screen transition animation",
  "Something long Something long Something long Something long Something long Something long",
];

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image Carousel',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return new MaterialApp(
       home: new DefaultTabController(
         length: 5,
         child:  Scaffold(
      appBar: AppBar(
        title: Text("Page View Demo Screen"),
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          // adding image carsoul
          ImageCarousel(),
          //adding horizental Buttons
          HorizentalButtons(),
         
          // adding ListView
          SizedBox(
            height: MediaQuery.of(context).size.height / 2,
            child: ContentList(notes),
          ),
          SizedBox(height: 10),
          VideoButtons(),
        ]),
      ),
    )
       ),
    );
   
  }
}

// Tab Bar Controller
class PageViewShow extends StatelessWidget {
  Widget build(BuildContext context) {
   return  PageView.builder(
     itemCount: 3,
     itemBuilder: (context, page) {
        return Center(child: Text('page 1'));
         },

       
   );
  }
}

// videos button
class VideoButtons extends StatelessWidget {
  Widget build(BuildContext context) {
    return GestureDetectorButton();
  }
}

//Horizental Buttons
class HorizentalButtons extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      margin: const EdgeInsets.only(left: 0.0, right: 0.0),
      decoration: new BoxDecoration(
          color: Colors.grey,
          border: Border(
            bottom: BorderSide(color: Color.fromRGBO(0, 83, 79, 1), width: 2.0),
            top: BorderSide(color: Color.fromRGBO(0, 83, 79, 1), width: 2.0),
            left: BorderSide(color: Color.fromRGBO(0, 83, 79, 1), width: 2.0),
            right: BorderSide(color: Color.fromRGBO(0, 83, 79, 1), width: 2.0),
          ),
          borderRadius: new BorderRadius.all(
            const Radius.circular(10.0),
          )),
      height: 200,
      width: 400,
      child: Column(
        children: <Widget>[
          Text('Welcome to etisalat App',
              style: TextStyle(
                fontSize: 16,
              )),
          SizedBox(height: 10),
          Container(
            width: 300,
            height: 5,
            color: Colors.black,
          ),
          SizedBox(height: 10),
         Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: 5),
          Expanded(
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0),
                side: BorderSide(color: Colors.red)),
                color: Colors.green,
                child: Text('Champions')),
          ),
          SizedBox(width: 10),
          Expanded(
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.red)),
                color: Colors.green[50],
                child: Text('Leagues')),
          ),
          SizedBox(width: 10),
          Expanded(
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.red)),
                color: Colors.green,
                child: Text('football')),
          ),
          SizedBox(width: 5),
        ],
      ),


        ],
      ),
    );
  }
}

//list
class ContentList extends StatelessWidget {
  // list of objects
  final List<String> notes;
  ContentList(this.notes);
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: notes.length,
      itemBuilder: (context, idx) {
        return ContentListTile();
      },
    );
  }
}

// carousel
class ImageCarousel extends StatelessWidget {
  final carousel = Carousel(
    boxFit: BoxFit.cover,
    images: [
      AssetImage('assets/images/images-2.jpeg'),
      AssetImage('assets/images/images.jpeg'),
    ],
    animationCurve: Curves.fastOutSlowIn,
    animationDuration: Duration(milliseconds: 2000),
  );
  final banner = new Padding(
    padding: const EdgeInsets.only(top: 10, left: 10),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.amber.withOpacity(0.5),
        //borderRaduis : BorderRadius.only(topLeft:Raduis.circular(10)),
      ),
      padding: EdgeInsets.all(10),
      child: Text('Text on carasoul',
          style: TextStyle(
            fontFamily: "",
            fontSize: 18,
          )),
    ),
  );

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 0),
      //padding: EdgeInsets.only(top:0),
      height: 200,
      child: new ClipRect(
        child: new Stack(
          children: <Widget>[
            carousel,
            banner,
          ],
        ),
      ),
    );
  }
}

