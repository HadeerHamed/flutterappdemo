import 'package:flutter/material.dart';
// cell
class ContentListTile extends StatelessWidget{
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: (){

          },   
          title:    Center(child:Text('the title')),
          subtitle: Center(child:Text('The Sub Title')),
           trailing: Column(
          children: <Widget>[
            Icon(Icons.comment),
            Text('TeamNumber one')
          ],
        ),
        leading:Column(
          children: <Widget>[
            Icon(Icons.access_alarm),
            Text('TeamNumbertwo')
          ],
        ), 
        ),
      ],
    );
  }
}